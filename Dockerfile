FROM adoptopenjdk/openjdk13:ubi
ENV APP_HOME=/usr/app/
WORKDIR $APP_HOME
COPY out/artifacts/jwork_jar/jwork-1.1.jar app.jar
EXPOSE 8080
CMD ["java", "-jar", "app.jar"]
